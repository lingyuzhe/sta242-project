\name{summary.AQI}
\alias{summary.AQI}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Summarize an AQI class object
}
\description{
Get the overall Air Pollution Level for each city in the given time period.
Or get a list of the summary statistics for each cities' specific AQI index.
}
\usage{
\method{summary}{AQI}(object, \dots)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{object}{An object of class AQI, displaying air quality indexes in custom locations and time interval.}
  \item{\dots}{Other input arguments.}
}
\details{
If the result of this function is assigned to a variable, quantile outputs can be saved. Otherwise, overall AQI will be printed. 
}
\author{
Jing Zhang
}
\examples{
data2=getAQI(city=c("Beijing","Shanghai","kunming"),
index=c("pm2.5","pm10"), from="2015-05-01", to="2015-06-03")
summary(data2)
}
\keyword{ summary }