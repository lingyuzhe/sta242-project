\name{ChinaMap}
\alias{ChinaMap}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Plot China map
}
\description{
This function generates China map on provincal level.
}
\usage{
ChinaMap()
}
%- maybe also 'usage' for other objects documented here.
\details{
Name of the province is located on its capital city. 
}
\author{
Lizhen Ji
}
\keyword{ plot_China_map }

