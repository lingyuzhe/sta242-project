library(RCurl)
library(ggplot2)
library(rgdal)
library(maptools)

getAQImap = function(day){
  
  ul1 = "http://data.biogeo.ucdavis.edu/data/gadm2/R/CHN_adm1.RData"
  ul2 = "http://data.biogeo.ucdavis.edu/data/gadm2/R/CHN_adm2.RData"
  uld1 = "https://bitbucket.org/lingyuzhe/sta242-project/raw/78701c8d241b809287253437e701b12b9aa580b7/AQI/"
  uld2 = "AQI.csv"
  urld = paste(uld1, day, uld2, sep = "")
  
  load(url(ul2))
  china.adm2.spdf = get("gadm")
  
  dat  = getURL(urld, .opts = list(ssl.verifypeer = FALSE))
  dat = read.csv(textConnection(dat),stringsAsFactors=FALSE)
  dat$City = as.character(dat$City)
  data.date = as.character(dat[,'Date'][1])
  dat$City = as.character(dat$City)
  dat$City[27] = china.adm2.spdf@data$NAME_2[311]
  
  #change city name in data for later matching
  dat[which(dat$City=='xian'),]$City = "xi'an"
  dat[which(dat$City=='haerbin'),]$City = "harbin"
  dat[which(dat$City=='huhehaote'),]$City = "hohhot"
  #delete hongkong, qingdao, shenzhen, ningbo, zhuhai,dalian, xiamen
  dat = dat[-c(30,31,34,35,36,37,38),]
  #change first character of city name to upper
  upper = function(str)
  {
    paste(toupper(substr(str, 1, 1)), substr(str, 2, nchar(str)), sep="")
  }
  dat$City= sapply(dat$City, upper)
  city = dat$City
  #add province to this data.frame 
  #find province corresponding with city
  findpro = function(city)
  {
    id = which(china.adm2.spdf@data$NAME_2==city)[1]
    china.adm2.spdf$NAME_1[id]
  }
  id= sapply(dat$City, findpro)
  dat[,'id'] = id
  #use province as ID to merge data frame
  dat.df = dat[,c(1,2,3,4,5,6,8,10)]

  china.centroids.df = data.frame(long = coordinates(china.adm2.spdf)[,1],lat = coordinates(china.adm2.spdf)[, 2]) 
  #id_1 represents province name, id_2 represents capital city name in province
  china.centroids.df[, 'ID_2']  = china.adm2.spdf@data[,'ID_2']
  china.centroids.df[, 'NAME_2'] = china.adm2.spdf@data[,'NAME_2']
  ind = which(china.centroids.df[,'NAME_2']%in%city)
  #there is one city have duplicate name, delete the non-capital one
  ind1 = which(china.centroids.df$ID_2==176)
  ind = ind[-which(ind1==ind)]
  china.centroids.df = china.centroids.df[ind,]
  #add province column to data
  china.centroids.df[,'id'] = sapply(china.centroids.df$NAME_2, findpro)
  
  #delete "Paracel Islands"
  load(url(ul1))
  data = fortify(gadm, region = "NAME_1")
  ind = which(data$id== "Paracel Islands")
  data = data[-ind,]
  
  #find unique boundary of each province
  province = unique(data$id)
  province1 = sapply(1:31, function(x) paste(province[x], ".1", sep = ""))
  index = which(data$group%in%province1)
  data.df = data[index, ]
  #merge two data.frame together
  data.df = merge(data.df, dat.df, by = 'id')
  res = list(data.df = data.df, china.centroids.df = china.centroids.df)
  res
  
}















