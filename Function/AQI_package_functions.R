library(XML)
library(RCurl)
library(foreign)
library(googleVis)

#get City name 
#capital cities in China 
#http://en.wikipedia.org/wiki/List_of_capitals_in_the_People%27s_Republic_of_China
ChinaMajorCity=
  function()
  {
    tmp = readHTMLTable("http://en.wikipedia.org/wiki/List_of_capitals_in_the_People%27s_Republic_of_China")
    tt = tmp[-1]
    #the below four tables are
    #province capitals 22
    #autonomous regions capitals, manicipalities & special administrative regions, sub-provincial cities
    #distract first column from tables
    city = sapply(tt, '[', 2)
    city = unlist(city, use.names = FALSE)
    city = as.character(city)
    
    city[which(city=="Xi'an")] = "xian"
    city[which(city=="Hong Kong")]="hongkong"
    city[which(city=="Harbin")]="haerbin"
    city[which(city=='Hohhot')]="huhehaote"
    city[which(city=='Macau')]="zhuhai" #change macau to zhuhai
    city[27]= "wulumuqi"
    city = tolower(city)
    city
  }  

ScrapingAQI_city =
  function (city)
  {
    url<-paste('http://aqicn.org/city/',city,'/m',sep="")
    rawtable=readHTMLTable(url)$table
    while(length(as.numeric(as.character(rawtable[,2][1:6])))!=6){
      url<-paste('http://aqicn.org/city/',city,'/m',sep="")
      rawtable=readHTMLTable(url)$table
    }
    rawtable = as.numeric(as.character(rawtable[,2][1:6]))
    names(rawtable)=c("PM2.5","PM10","O3","NO2","SO2","CO")
    rawtable
  }


ScrapingAQI_daily =
  function()
  {
    city = ChinaMajorCity()
    data=sapply(city,ScrapingAQI_city)
    data=as.data.frame(t(data))
    data['AQI'] = apply(data, 1, max)
    data["City"]=rownames(data)
    data["Date"]=Sys.Date()
    data
  }

#Example
#city = ChinaMajorCity()
#beijing = ScrapingAQI_city("beijing")
#this function may take long since (no NA allowed)
#day0610 = ScrapingAQI_daily()

##### getAQI #####
getAQI = 
  function(city=c("beijing","shanghai"), index=c("PM2.5","PM10"), 
           from="2015-05-01", to="2015-05-10"){
  # Args:
  #   from: character string representing the start date
  #   to:   character string representing the end date
  giturl = "https://bitbucket.org/lingyuzhe/sta242-project/raw/36bb75e4f8a4c941980f8a2f61b5f003a7d08aaf/AQI"
  timeint = seq(as.Date(from), as.Date(to), by = 'day')
  url = paste(giturl, paste(paste0(timeint, "AQI"), "csv", sep = ".") , sep = '/')
  data <- getURL(url, .opts = list(ssl.verifypeer = FALSE))
  data = read.csv(textConnection(data),stringsAsFactors=FALSE)
  city=tolower(city)
  index=toupper(index)
  data = data[data$City %in% city, c(index, "AQI","City", "Date")]
  data[c(index,"AQI")] = sapply(data[c(index,"AQI")],as.numeric)
  data$Date = as.Date(data$Date)
  rownames(data) = NULL
  structure(data,class=c("AQI","data.frame"))
  }

##examples###

#data1=getAQI(city=c("beijing","shanghai"), index=c("PM2.5","PM10","O3"), 
#           from="2015-05-01", to="2015-06-03")

#data2=getAQI(city=c("Beijing","Shanghai","kunming"), index=c("pm2.5","pm10"), 
#         from="2015-05-01", to="2015-06-03")

##### plot.AQI summary.AQI (S3 method)#####

plot.AQI = 
  function(x,...)
  {
    data = gvisMotionChart(x,idvar = "City", timevar = "Date")
    plot(data)
  }

SuSd=
  function(x)
  {
    std = sd(x)
    names(std) ="std."
    c(summary(x),std)
  }

AQItoAPL =
  function(AQI)
{
  if (AQI<= 50)  "Excellent"
  if (AQI > 50 && AQI <=100) return("Good")
  if (AQI > 100 && AQI<=150) return("Lightly Polluted")
  if (AQI > 150 && AQI<=200) return("Moderately Polluted")
  if (AQI > 200 && AQI<=300) return("Heavily Polluted")
  if (AQI > 300) return("Severely Polluted")
  }

summary.AQI =
  function(object,...)
  {
    
    IndexN = dim(object)[2]-3
    City = unique(object$City)
    CityTableList=lapply(City, function(x) subset(object,City == x))
    
    CityAQIMean = sapply(CityTableList, function(x) mean(x$AQI))
    names(CityAQIMean) = City
    CityAPL = sapply(CityAQIMean,AQItoAPL)
    
    print=sapply(City, function(x) 
      cat(sprintf("%s Average AQI: %g, Air Pollution Lever: %s\n",
                                               toupper(x),CityAQIMean[[x]],CityAPL[[x]])))
    
    CitySummaryList=lapply(CityTableList, 
                           function(x) as.data.frame(
                             lapply(subset(x,select=1:IndexN),SuSd)))
    
    CitySummaryList=lapply(CityTableList, 
                           function(x) 
                             lapply(subset(x,select=1:IndexN),SuSd))
    
    names(CitySummaryList) = City
    list=CitySummaryList
  }

##examples###

#plot(data1)
#summary(data1)
#plot(data2)
#summary(data2)

#test1 = summary(data1)
#test1$beijing$PM2.5

#test2 = summary(data2)
#test2$beijing$PM2.5
