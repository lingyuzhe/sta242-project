plotmap = function(date, pollutant, data, col){
  data.df = data[[1]]
  china.centroids.df = data[[2]]
  ptitle = paste(pollutant, date, sep = " ")
  ind = which(colnames(data.df)==pollutant)
  colnames(data.df)[ind]="pollutant"
  p = ggplot(data.df, aes(x = long, y = lat, group = id)) +  geom_polygon(aes(fill = cut(pollutant,9))) +
    geom_text(data =china.centroids.df , aes(label = NAME_2, x = long, y = lat, group = NAME_2), size = 2.5) + 
    labs(x=" ", y=" ") + 
    theme_bw() + scale_fill_brewer(pollutant, palette  = col) + 
    coord_map()+ggtitle(ptitle)+
    theme(panel.grid.minor=element_blank(), panel.grid.major=element_blank()) + 
    theme(axis.ticks = element_blank(), axis.text.x = element_blank(), axis.text.y = element_blank()) + 
    theme(panel.border = element_blank())  
  p 
}






